var app = {
    temp: {
        windowWidth: null
    }
}
var navbar = {
    object:'#main-navbar',
    clone:'#clone-navbar',
    cloneDisplayed:false,
    defaultPadding: 15
};
navbar.initialize = function(){
    this.defaultPadding = parseFloat($(this.object+' .navbar-nav:first li:first a').css('padding-right'));
    this.bindWidth();
}

$(document).ready(function($){
    $('.search-btn').click(function(){ // Цвет стрелки возле поиска
            $('.searchInput-wrapper').toggle();
            $('.search-btn>.caret').css('color',
                ($('.searchInput-wrapper').css("display")=="none")?'#999999':'#ffffff');

    });
    $('.search-x').click(function(){
        $('.searchInput-wrapper').hide();
        $('.search-btn>.caret').css('color', '#999999');
    });

    $('#main-navbar>div>button').click(function(){
        var nav = $('#navbar-collapse-2');
        //if($(nav).height()==0)
            $(nav).stop().slideToggle;
    });

    $('.footer').hover(function(){
        $('.footer-inner').stop().slideDown();
    }, function(){
        $('.footer-inner').stop().slideUp();
    });
    //navbar.initialize();
    setTimeout(navbar.initialize(), 500);
    setTimeout(normalizeBody(), 2000);
});

$(window).resize(function(){
    normalizeBody();
});

function normalizeBody(){
    //$('body>div').height($(window).height());
    /*if($(window).width() <= 768){
        $('.search-bar').css('display', '');
    }*/
    if($(window).width() != app.temp.windowWidth){
        navbar.toTLines2();
        /*setTimeout(function(){
            if(navbar.stretch()){
                //$(navbar.object+' .navbar-brand').attr('data-toggle', '');
                /*$(navbar.object+' .navbar-nav:first').width(navbar.getAvailableLNS());*/
            /*} else {
                //$(navbar.object+' .navbar-brand').attr('data-toggle', 'collapse');
            }
        }, 10);
*/
        app.temp.windowWidth = $(window).width();
    }


}
/*function normalizeNavnar(){
    var available = 0;
    available = $(window).width();
    if(available<770) return;
    var navbar = $('.navbar')[0];
    var container = navbar.children('.container-fluid')[0];
    available -= navbar.width() - container.width(); // расстояние между навом и контейнером
    dif += container.width() - container.children('navbar-header').width();
    var el = $('.navbar>.container-fluid')[0];
    var x = el.paddingLeft + el.paddingRight + el.marginLeft+el.marginRight;
}*/

/* === NavBar === */
navbar.bindWidth = function(){
    var nava = $(this.object+' .navbar-left>li>a');
    for(var i = 0; i < nava.length; i++){
        $(nava[i]).attr('data-width', $(nava[i]).width());
    }
};

navbar.getAvailableLNS = function(obj){
    var container = $((obj?obj:this.object)+'>.container-fluid');
    var nbR = container.children('.collapse').children('.navbar-right')[0];
    return $(container).children('.collapse').width() -
        $(container).children('.navbar-header').width() -
        (nbR ? $(nbR).width() : 0) -
        (nbR ? parseFloat($(nbR).css('margin-right')) : 0)-0.5;
};
navbar.getNLW = function(){
    return $('.navbar-left').width();
}
navbar.getCleanWidth = function(obj){
    var nav = $((obj?obj:this.object)+' .navbar-nav:first>li');
    var result = 0;
    var a;
    for(var i = 0; i<nav.length; i++){
        a = $(nav[i]).children('a');
        result += $(a).width() + (parseFloat($(a).css('border-left-width')) + parseFloat($(a).css('border-right-width')));
    }
    return result.toFixed(3)-0;
}
navbar.getOverflowers = function(skipLast){
    var nwidth = this.getNLW() - this.getCleanWidth();
    var nav = $(this.object+' .navbar-nav:first>li');
    var stack = [];
    for(var i = nav.length-(skipLast?1:0); i > 0 && nwidth < 0 && $(window).width() > 768; i--){
        nwidth += $(nav[i-1]).width()-
            (parseFloat($(nav[i-1]).children('a').css('padding-left')) -0
            + parseFloat($(nav[i-1]).children('a').css('padding-right'))).toFixed(3);
            stack.push(nav[i-1]);
    }
    return stack.reverse();
};
navbar.getOverflowers2 = function(skipLast){
    var nwidth = $(window).width() - ($('#main-navbar.overflowed').length==1 ? 178 : 195) - navbar.getNLW();
    var nav = $(this.object+' .navbar-left>li');
    var stack = [];
    for(var i = nav.length-(skipLast?1:0); i > 0 && nwidth < 0 /*&& $(window).width() > 768*/; i--){
        nwidth += $(nav[i-1]).width()-
            (parseFloat($(nav[i-1]).children('a').css('padding-left')) -0
            + parseFloat($(nav[i-1]).children('a').css('padding-right'))).toFixed(3);
        stack.push(nav[i-1]);
    }
    return stack.reverse();
};
navbar.getClimbs = function(){
    var nwidth = this.getNLW() - this.getCleanWidth();
    var drops = $(this.object+' .navbar-nav:first>li.dropdown.more>ul>li');
    var stack = [];
    for(var i = 0; i < drops.length && nwidth > 0 && $(window).width() > 768; i++){
        nwidth -= $(drops[i]).width()-
        (parseFloat($(drops[i]).children('a').css('padding-left')) -0
        + parseFloat($(drops[i]).children('a').css('padding-right'))).toFixed(3);
        if(nwidth > 0)
        stack.push(drops[i]);
    }
    return stack;
};
navbar.getLNBDataWidth = function(){
    var result = 0;
    var nav = $(this.object+' .navbar-left>li>a');
    for(var i = 0; i < nav.length; i++){
        result += ($(nav[i]).attr('data-width') -0) +2;
    }
    return result;
}
navbar.getAddClimbs = function(){
    var nwidth = navbar.getNLW() - navbar.getLNBDataWidth();
    var drops = $(this.object+' #navbar-collapse-2>.navbar-nav>li');
    var stack = [];
    for(var i = 0; i < drops.length && nwidth > 0 /*&& $(window).width() > 768*/; i++){
        nwidth -= $(drops[i]).children('a').attr('data-width')-0;
        if(nwidth > 0)
            stack.push(drops[i]);
    }
    return stack;
}
navbar.exchangeWithClone = function(){
    var cloneCh = $(this.clone+' .navbar-nav:first li'); // получили детей клона
    var move = []; // создали массив для перемещения
    var available = (this.getAvailableLNS()-this.getCleanWidth()); // получили доступное пространство
    if(
        (cloneCh.length > 0) &&
        (available > ($($(cloneCh[0]).children('a')[0]).width()+(this.defaultPadding*2)))
    ) {
        for(var i = 0; i < cloneCh.length && available > (parseFloat($(cloneCh[i]).width())+(this.defaultPadding*2));
            i++, available -= $($(cloneCh[0]).children('a')[0]).data('mWidth')+(this.defaultPadding*2)){
            $($(cloneCh[0]).children('a')[0]).data('mWidth');
            $(this.object+' .navbar-nav:first').append($(cloneCh[i])); // !!!!!!!!!!!!!!! Insert
        }
    } else {
        move = this.getOverflowers();
        for(var i = 0; i < move.length; i++){
            $($(move[i]).children('a')[0]).data('mWidth',$($(move[i]).children('a')[0]).width());
            $(this.clone+' .navbar-nav:first').append($(move[i]));
        }
    }
    navbar.do();
    navbar.do(this.clone);
}
navbar.do = function(obj){
    if(!obj) obj = this.object;
    var nav = $(obj+' .navbar-nav:first li');
    var over = this.getAvailableLNS(obj) - this.getCleanWidth(obj);
    var add = parseFloat((over / nav.length).toFixed(2) / 2);
    var cPadding = this.defaultPadding + add;
    for(var i=0; i < nav.length; i++){
        if($(nav[i]).css('display') == 'none') $(nav[i]).css('display', 'block');
        $(nav[i]).children('a').css('padding-left', cPadding);
        $(nav[i]).children('a').css('padding-right', cPadding);
    }
    over = this.getAvailableLNS(obj) - this.getCleanWidth(obj);
    if(over < 0) {
        var a = $(nav[i]).children('a');
        $(a).css('padding-left', parseFloat($(a).css('padding-left')) + (over/2));
        $(a).css('padding-right', parseFloat($(a).css('padding-right')) + (over/2));
    }
}
navbar.hideExcess = function(){
    var nwidth = this.getAvailableLNS();
    if(nwidth >= $(this.object+' .navbar-nav:first').width())
        return nwidth - $(this.object+' .navbar-nav:first').width();
    var nav = $(this.object+' .navbar-nav:first').children('li');
    var i;
    var swidth = 0;
    var over = false;
    for(i = 0; i < nav.length; i++){
        if(!over) {
            swidth += $(nav[i]).width();
        }
        if(swidth >= nwidth && $(window).width() > 768){
            $(nav[i]).css('display', 'none');
            if(!over){
                over = nwidth - (swidth - $(nav[i]).width());
            }
        } else {
            $(nav[i]).css('display', '');
        }
    }
    return over;
};
navbar.stretch = function(){
    var nav = $(navbar.object+' .navbar-nav:first>li>a');
    var over = navbar.getNLW() - navbar.getCleanWidth();
    if($(window).width()<=768)
        nav.css('padding-left', '')
        .css('padding-right', '');
    if(over < 0 || $(navbar.object+' .collapse').css('display')=='none') return false;
    var add = ((over / nav.length) / 2).toFixed(3) -0.15;
    nav.css('padding-left', add)
          .css('padding-right', add);
    return true;
};
navbar.bindNavbarBrand = function(){
    if($(window).width() <= 768) {
        if (!$(this.object + ' .navbar-brand').data('toggle') != 'collapse')
            $(this.object + ' .navbar-brand').data('toggle', 'collapse');
    }
    else {
        $(this.object + ' .navbar-brand').data('toggle', 'collapse');
    }
}
navbar.toTLines = function(){
    var over = navbar.getNLW() - navbar.getCleanWidth();
    var dropdown = $(this.object+' #navbar-collapse-2>.navbar-nav');
    if(over <= 0 || $(dropdown).children('li').length != 0){
        $(this.object).addClass('overflowed');
    } else {
        $(this.object).removeClass('overflowed');
    }

    if($(window).width()<=768){
        $(this.object+' .navbar-left').append($(dropdown.children('li')));
        $(this.object).removeClass('overflowed');
        return;
    }
    var stack = [];
    if(over > 0) {
        stack = this.getAddClimbs();
        $(stack).children('a').attr('data-width','');
        $(this.object+' .navbar-left').append(stack);
    }
    else {
        stack = this.getOverflowers();
        for(var li in stack) {
            $(stack[li]).children('a').attr('data-width',($(stack[li]).children('a').width()));
        }
        $(dropdown).prepend(stack);
    }
    if($(dropdown).children('li').length == 0)
    ;
        //dropdown.remove();
};
navbar.toTLines2 = function(){
    var over = $(window).width() - ($('#main-navbar.overflowed').length==1 ? 178 : 194) - navbar.getNLW();
    var dropdown = $(this.object+' #navbar-collapse-2>.navbar-nav');
    if($('#main-navbar.overflowed').length==1 && over > 0 && (navbar.getNLW() - navbar.getLNBDataWidth()) < 30 && $(dropdown).children('li').length == 0)
        return true;
    if(over > 0 && $(dropdown).children('li').length == 0){
        $(this.object).removeClass('overflowed');
        $('.searchInput-wrapper').removeClass('search-top');
    } else {
        $(this.object).addClass('overflowed');
        $('.searchInput-wrapper').addClass('search-top');
    }

    /*if($(window).width()<=768){
        $(this.object+' .navbar-left').append($(dropdown.children('li')));
        $(this.object).removeClass('overflowed');
        return;
    }*/
    var stack = [];
    if(over > 0) {
        stack = this.getAddClimbs();
        //$(stack).children('a').attr('data-width','');
        $(this.object+' .navbar-left').append(stack);
    }
    else {
        stack = this.getOverflowers2();
        for(var li in stack) {
            $(stack[li]).children('a').attr('data-width',($(stack[li]).children('a').width()));
        }
        $(dropdown).prepend(stack);
    }
    if($(dropdown).children('li').length == 0)
        ;
    //dropdown.remove();
};
navbar.toDropdown = function(){
    var over = navbar.getAvailableLNS() - navbar.getCleanWidth();
    var dropdown = $(this.object+' .navbar-nav:first li.dropdown.more');
    if($(window).width()<=768){
        $(dropdown.children('ul').children('li')).insertBefore(dropdown);
        dropdown.remove();
        return;
    }
    if(dropdown.length != 1) {
        dropdown = $(document.createElement('li')).addClass('dropdown').addClass('more')
            .append([$(document.createElement('a'))
                .addClass('dropdown-toggle')
                .attr('href', '#')
                .attr('data-toggle', 'dropdown')
                .data('text', 'more')
                .attr('role', 'button')
                .attr('aria-expanded', 'false')
                .text('Ще ')
                .append($(document.createElement('span')).addClass('caret')),
                $(document.createElement('ul')).addClass('dropdown-menu')
                    .attr('role', 'menu')]
        );
        $(this.object+' .navbar-nav:first').append(dropdown);
    }
    var stack = [];
    if(over > 0) {
        stack = this.getClimbs();
        $(stack).width('');
        $(stack).insertBefore(dropdown);
    }
    else {
        stack = this.getOverflowers(true);
        for(var li in stack) {
            $(stack[li]).css('width',($(stack[li]).width()));
        }
        $(dropdown).children('ul').prepend(stack);
    }
    if($(dropdown).children('ul').children('li').length == 0)
        dropdown.remove();
}
function bindWidth(arr){
    var a;
    for(var i = 0; i < arr.length; i++){
        a = $(arr[i]).children('a');
        $(a).data('width', $(a).width())
    }
}

function stretch(obj){
    var nav = $(obj).children('*');
    var swidth = 0;
    for(var i = 0; i<nav.length; i++){
        swidth += $(nav[i]).width() + ($(nav[i]).css('border-left-width')-0) + ($(nav[i]).css('border-right-width')-0);
    }
    var over = $(obj).width() - swidth;
    if(over < 0) return false;
    var add = ((over / nav.length) / 2).toFixed(3) -0.05;
    nav.css('padding-left', add)
        .css('padding-right', add);
    return true;
}

