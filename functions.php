<?php

function knuca_scripts(){
	wp_enqueue_style( 'bootstrap.min', get_template_directory_uri().'/css/bootstrap.min.css', array(), '3.3.6', 'all' );
	wp_enqueue_style( 'style', get_template_directory_uri().'/style.css', array('bootstrap.min'), '1.0.0', 'all' );
	wp_enqueue_script( 'jquery.min', get_template_directory_uri().'/js/jquery.min.js', array(), '1.11.2', true );
	wp_enqueue_script( 'bootstrap.min', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery.min'), '3.3.6', true );
	wp_enqueue_script( 'main', get_template_directory_uri().'/js/main.js', array('jquery.min'), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'knuca_scripts' );
