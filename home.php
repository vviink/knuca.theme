<?php get_header(); ?>
    <div class="main-page">
        <h1>КНУСА</h1>
        <h3><span class="p1">Киевский Национальный Университет</span> <span class="p2">Строительства и Архитектуры</span></h3>
        <div class="lang">
            <ul class="line-list">
                <li class="ua flag-right l1 active"><a href="/?lang=ua">UK</a></li>
                <li class="en flag-left l2"><a href="/?lang=en">EN</a></li>
                <li class="ru flag-right l3"><a href="/?lang=ru">RU</a></li>
            </ul>
        </div>
    </div>
<?php get_footer(); ?>