<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <title>КНУСА</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-Control" content="no-store" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
    <style>
        #splash{
            width: 100%;
            height: 100%;
            position: absolute;
            background: url("<?php echo get_template_directory_uri(); ?>/images/loading.gif") center no-repeat;
            background-size: cover;
            z-index: 1035;
            -webkit-transition: opacity 3s;
            -moz-transition: opacity 3s;
            -ms-transition: opacity 3s;
            -o-transition: opacity 3s;
            transition: opacity 3s;

        }
    </style>
    <script>
        var splashTimeout = setTimeout(function(){document.getElementById("splash").style.opacity=0;removeTimeout()}, 2000);
        var removeTimeout = function(){setTimeout(function(){document.getElementById("splash").remove();}, 3000);};
        document.onload += function(){clearTimeout(splashTimeout);document.getElementById("splash").style.opacity=0;removeTimeout()};
    </script>
</head>
<body>
<div id="splash"></div>
<div class="body-div">
<nav class="navbar navbar-fixed-top" id="main-navbar">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="search-btn"><div class="lens-ico" aria-hidden="true"></div></a>
            <a class="navbar-brand" href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"> </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=" navbar-collapse" >
            <div class="lnb">
            <ul class="nav navbar-nav navbar-left">
                <li class="first">
                    <a href="pro-universitet/" >Про університет</a>
                </li>
                <li>
                    <a href="abiturientam/" >Абітурієнтам</a>
                </li>
                <li>
                    <a href="naukova-robota.html" >Наукова робота</a>
                </li>
                <li>
                    <a href="novini.html" >Новини</a>
                </li>
                <li>
                    <a href="elektronna-biblioteka.html" >Електронна бібліотека</a>
                </li>
                <li>
                    <a href="kontakti.html" >Контакти</a>
                </li>
                <li>
                    <a href="studentam.html" >Студентам</a>
                </li>
                <li class="last">
                    <a href="mizhnarodnij-viddil.html" >Міжнародний відділ</a>
                </li>
            </ul>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="search-btn"><div class="lens-ico" aria-hidden="true"></div> ПОИСК <span class="caret"></span></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-collapse-2">
            <ul class="nav navbar-nav navbar-overflowed">

            </ul>
        </div>
    </div><!-- /.container-fluid -->
</nav>
<form action="/search" method="get">
    <div  class="searchInput-wrapper search-bar">
            <input type="text" class="form-control" name="search" id="search"  placeholder="ПОИСК...">
            <div class="search-x" id="close-addon"></div>
    </div>
    <input type="hidden" name="id" value="1" />
    <!-- <input type="submit" value="Search" /> -->
</form>

<div class="container content">